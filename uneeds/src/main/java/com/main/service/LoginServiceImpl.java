package com.main.service;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.main.domain.uMemberVO;
import com.main.persistence.LoginDAO;

@Service
public class LoginServiceImpl implements LoginService{
	
	@Inject
	LoginDAO loginDao;
	
	@Override
	public String getTime() {
		return loginDao.selectTime();
	}


	@Override
	public int login(String id, String site) {
		
		return loginDao.login(id, site);
	}
	

}
