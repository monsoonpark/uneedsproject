package com.main.domain;

public class LoginVO {
	
	//
	private String mid;
	private String lname;
	private int usercode;
	public LoginVO(String mid, String lname, int usercode) {
		super();
		this.mid = mid;
		this.lname = lname;
		this.usercode = usercode;
	}
	public LoginVO() {
		super();
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public int getUsercode() {
		return usercode;
	}
	public void setUsercode(int usercode) {
		this.usercode = usercode;
	}
	
	
}
