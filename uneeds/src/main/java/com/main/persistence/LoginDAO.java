package com.main.persistence;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import com.main.domain.uMemberVO;

public interface LoginDAO {
	
	public String selectTime();
	
	//로그인
	public int login(String id, String site);
	
}
