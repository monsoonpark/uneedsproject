package com.main.persistence;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.main.domain.LoginVO;

@Repository
public class LoginDAOImpl implements LoginDAO{

	@Inject SqlSession sqlSessionPub;
	
	private static final String namespace="com.main.mappers.MainMapper";
	
	
	//현재 시간
	@Override
	public String selectTime() {
		return sqlSessionPub.selectOne(namespace+".getTime");
	}



	@Override
	public int login(String id, String site) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mid", id);
		map.put("lname", site);
		sqlSessionPub.selectOne(namespace+".login", map);
		int result = (int) map.get("usercode");
		System.out.println("usercode: "+result);
		return result;
	}

	
}
