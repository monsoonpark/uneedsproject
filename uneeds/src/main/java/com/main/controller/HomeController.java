package com.main.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.main.service.LoginService;

/**
 * Handles requests for the application home page.
 */
@Controller("HomeController")
//@RequestMapping(value = "/uneeds")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Inject
	LoginService lservice;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	// 메인 controller
	// http://localhost:8080/uneeds/
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView home(Locale locale) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		ModelAndView mav= new ModelAndView();
		mav.setViewName("uneeds_main");
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		mav.addObject("serverTime", formattedDate );
		
		return mav;
	}
	
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String totalLogin(HttpServletRequest req, @RequestParam String id, @RequestParam String site) {
		HttpSession session= req.getSession();
		session.setAttribute("login", "logined");
		session.setAttribute("id", id);
		session.setAttribute("site", site);
		session.setAttribute("usercode", lservice.login(id, site));
		System.out.printf("사이트: %s, 아이디: %s",site, id);
		
		// 이전페이지 url가져오기
		String referer = req.getHeader("Referer");
		return "redirect:"+referer;
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String totalLogin(Locale locale, Model model) {
		
		return "totalLogin";
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String totalLogout(HttpServletRequest req) {
		HttpSession session= req.getSession();
		session.invalidate();
		
		//이전 url로 되돌아가기
		String referer = req.getHeader("Referer");
		return "redirect:"+referer;
	}
	
	@RequestMapping(value = "chat", method = RequestMethod.GET)
	public String chat(Locale locale, Model model) {
		return "chat";
	}
	
	
	
	/*
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public String homet(Locale locale, Model model) {
		return "mains";	
	}
	
	@RequestMapping(value = "test/test", method = RequestMethod.GET)
	public String homete(Locale locale, Model model) {
		return "mains";
	}
	*/
	
	
}
