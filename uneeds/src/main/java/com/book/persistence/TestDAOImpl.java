package com.book.persistence;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class TestDAOImpl implements TestDAO{

	@Inject SqlSession oraclesqlSession;
	@Inject SqlSession mysqlSession;
	
	private static final String namespace="com.book.mappers.TestMapper";
	
	@Override
	public String selectTime() {
		return mysqlSession.selectOne(namespace+".getTime");
	}

}
