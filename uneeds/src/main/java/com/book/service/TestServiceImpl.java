package com.book.service;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.book.persistence.TestDAO;

@Service
public class TestServiceImpl implements TestService{
	
	@Inject
	private TestDAO dao;
	
	@Override
	public String getTime() {
		return dao.selectTime();
	}
}
