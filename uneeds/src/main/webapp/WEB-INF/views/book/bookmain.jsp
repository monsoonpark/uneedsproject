<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Book</title>
<!-- jquery / bootstrap / js-->
<script src="//code.jquery.com/jquery-latest.js"></script> <!-- must be top -->
<script src="/resources/book/js/jquery.min.js"></script>
<script src="/resources/book/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/book/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- css -->
<link href="/resources/book/css/modern-custom.css" rel="stylesheet"/>
<link href="/resources/book/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="/resources/book/css/login.css" rel="stylesheet"/>
<link href="/resources/book/css/animate.css" rel="stylesheet"/>
<link href="/resources/book/css/bookList.css" rel="stylesheet"/>
<!-- swipe testing... -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
<script src="/resources/book/js/book.js"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
<!-- <script src="../resources/book/js/bootstrap.bundle.min.js"></script> -->
<!-- 구글폰트 -->
<link href="https://fonts.googleapis.com/css?family=Black+Han+Sans" rel="stylesheet">
<!-- Font Awesome - Glyphicons 대용 -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<script type="text/javascript">
var showTimes;	
var currentPosition;
$(function(){
	currentPosition = parseInt($("#right_section").css("top"));  

	showTimes = setInterval(function() {	// 1.000초마다 내부 실행
		//alert('http://webisfree.com');
		//$(".realTime").value="${serverTime}";
				
	}, 1000);
	clearInterval(showTimes);	// showTimes 함수 정지
	
});
// 스크롤 시
$(window).scroll(function() {
	// 스크롤이 바닥에 닿으면
	/*
	if ($(window).scrollTop() == $(document).height() - $(window).height()) {
	};
    */
   
	var position = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.  
	$("#right_section").stop().animate({"top":position+currentPosition+"px"},100);  
   
});


//모바일 감지
/*
function findBootstrapEnvironment() {
	var envs = ['xs', 'sm', 'md', 'lg'];
	
	var point = $(".pointed");
	point.appendTo($('body'));
	
	for (var i = envs.length - 1; i >= 0; i--) {
		var env = envs[i];
		
		point.addClass('hidden-'+env);
		if (point.is(':hidden')) {
			point.remove();
		return env;
		}
	}
}
*/

</script>
<style type="text/css">
body{
	background: url('/resources/book/img/backgroundbook.jpg') no-repeat repeat;
	background-size: 100%;
	background-position: center 65%;
}
body {
	padding-bottom: 0;
}
</style>
</head>
<body>
	<!-- ${pageContext.request.contextPath} 프로젝트 webapp까지의 경로 -->
	
	<div id="right_section" class="container pointed">  
    	<div class="row mb-4">
			<div class="col-md-2" style="background-color: skyblue; opacity: 0.5;">
				hello
			</div>
		</div>
    </div>  
    
	<!-- Navigation include -->
	<jsp:include page="/WEB-INF/views/book/common/navbar.jsp"></jsp:include>
	<!-- carousel 슬라이드 -->
	<header>
		<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
			<!-- 슬라이드 버튼 -->
			<ol class="carousel-indicators">
				<li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselIndicators" data-slide-to="1"></li>
				<li data-target="#carouselIndicators" data-slide-to="2"></li>
			</ol>
			<!-- 슬라이드 -->
			<div class="carousel-inner" role="listbox">
				<!-- Slide One - Set the background image for this slide in the line below -->
				<!-- search -->
				<div class="carousel-item active" onclick="location.href='search'"
					style="background-image: url('${pageContext.request.contextPath}/resources/book/img/bookalot.jpeg')">
					<!-- 설명 -->
					<div class="carousel-caption d-none d-md-block">
						<h3>도서 검색</h3>
						<h3>SERACH FOR WHAT U NEED!</h3>
						<label class="realTime">
							${serverTime}<br>
							${time}<br/>
						</label>
					</div>
				</div>
				<!-- seller -->
				<div class="carousel-item" onclick="location.href='bestseller'"
					style="background-image: url('../resources/book/img/booksale.jpg')">
					<div class="carousel-caption d-none d-md-block">
						<h3>잘나가는 도서</h3>	
						<h3>Best n Steady</h3>	
						<label>See wat kinda books r here</label>
					</div>
				</div>
				<!-- store map -->
				<div class="carousel-item" onclick="location.href='/uneeds/book/store'"
					style="background-image: url('../resources/book/img/bookshop.jpg');">
					<div class="carousel-caption d-none d-md-block">
						<h3>서점 지도</h3>
						<label>Book Store</label>
					</div>
				</div>
				
			</div>
			<!-- 좌우 화살표 버튼 -->
			<a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev"> 
				<span class="carousel-control-prev-icon" aria-hidden="true"></span> 
				<span class="sr-only">Previous</span>
			</a> <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span> 
				<span class="sr-only">Next</span>
			</a>
		</div>
	</header>

	<!-- Page Content -->
	<div class="container" id="background">

		<h1 class="my-4 animated animatedFadeInUp fadeInUp">WELCOME!</h1>
		<!-- 검색창 -->
		<!-- 
		<div class="col-md-12">
			<div class="row mb-4">
				<form action="#" method="get">
					<input value='' class="searcher" placeholder="검색 입력후 엔터" autofocus>
				</form>
				<hr class="searcherLine"/>
			</div> 
		</div>
		 -->
		<div class="row">
			
		</div>
		<!-- /.row -->
		
		<hr/>

		<!-- Call to Action Section -->
		<div class="row mb-4">
			<div class="col-md-8">
				<p class="fadeInUp">
					인내할 수 있는 자는 그가 원하는 결과를 얻을 수 있다. - 벤자민 프랭클린 - 
				</p>
				<hr/>
			</div>

			<div class="main-book animated animatedFadeInUp fadeInUp">
				<div class="col-md-12 my-auto py-auto">
					<h2>인기도서 목록</h2>
					<div class="row mb-4 my-auto mx-auto py-3">
						<c:forEach items="${countbest }" var="c">
							<div class="col-md-2 my-auto mx-auto py-auto px-auto">
								<img src="${c.bimage }" class="img" />
								<a href="/uneeds/book/info/${c.bisbn }">${c.btitle }</a>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>

			<div class="col-md-12" style="height:400px;"></div>
			
		</div>
	</div>
	<!-- /.container -->
	
	<!-- Footer include -->
	<jsp:include page="/WEB-INF/views/book/common/footer.jsp" flush="true"></jsp:include>

	
</body>
</html>